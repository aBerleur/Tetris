package _2048;

import AI.Decision;
import AI.State;
import java.util.ArrayList;
import java.util.List;

public class State2048 implements State{
    private Tile[] board;
    public char action;
    
    public State2048() {
        board = new Tile[16];
        for (int i = 0; i < board.length; i++) {
            this.board[i] = new Tile(0);
        }
    }

    public State2048(Tile[] board) {
        this.board = new Tile[16];
        for (int i = 0; i < board.length; i++) {
            this.board[i] = new Tile(board[i].value);
        }
    }
    
    public State2048(State2048 state) {
        this.board = state.board;
        for (int i = 0; i < board.length; i++) {
            this.board[i] = new Tile(state.board[i].value);
        }
        this.action = state.action;
    }
    
    @Override
    public List<State> getAllPossibleState() {
        List<State> allFuturState = new ArrayList<State>();
        State2048 state = getLeft();
        if (!state.equals(this)) {
            state.action = 'A';
            allFuturState.add(state);
        }
        state = getRight();
        if (!state.equals(this)) {
            state.action = 'D';
            allFuturState.add(state);
        }
        state = getTop();
        if (!state.equals(this)) {
            state.action = 'W';
            allFuturState.add(state);
        }
        state = getBottom();
        if (!state.equals(this)) {
            state.action = 'S';
            allFuturState.add(state);
        }
        return allFuturState;
    }
    
    public State2048 getRight() {
        State2048 newState = new State2048();
        int[] numberOrder = new int[4];
        
        for (int rows = 0; rows < 4; rows++) {
            numberOrder = new int[4];
            int j = 0;
            for (int i = (4 * rows) + 0; i < (4 * rows) + 4; i++) {
                if (board[i].value != 0) {
                    numberOrder[j] = board[i].value;
                    j++;
                }
            }
            for (int i = numberOrder.length - 1; i > 0 ; i--) {
                if (numberOrder[i] == numberOrder[i - 1]) {
                    numberOrder[i] *= 2;
                    numberOrder[i-1] = 0;
                }
            }
            j = (4 * rows) + 3;
            for (int i = numberOrder.length-1; i >= 0; i--) {
                if (numberOrder[i] != 0) {
                    newState.board[j].value = numberOrder[i];
                    j--;
                }
            }
        }
        return newState;
    }
    
    public State2048 getLeft() {
        board = rotate(180,board);
        State2048 newState = getRight();
        newState.board =  rotate(180,newState.board);
        board = rotate(180,board);
        
        return newState;
    }
    
    public State2048 getBottom() {
        board = rotate(270,board);
        State2048 newState = getRight();
        newState.board =  rotate(90,newState.board);
        board = rotate(90,board);
        
        return newState;
    }
    
    public State2048 getTop() {
        board = rotate(90,board);
        State2048 newState = getRight();
        newState.board =  rotate(270,newState.board);
        board = rotate(270,board);
        
        return newState;
    }

    @Override
    public Decision takeDecision(State finalState) {
        return new Decision2048(((State2048)finalState).action);
    }
    
    public Tile[] rotate(int angle, Tile[] tile) {
        Tile[] newTiles = new Tile[4 * 4];
        int offsetX = 3, offsetY = 3;
        if (angle == 90) {
            offsetY = 0;
        } else if (angle == 270) {
            offsetX = 0;
        }

        double rad = Math.toRadians(angle);
        int cos = (int) Math.cos(rad);
        int sin = (int) Math.sin(rad);
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                int newX = (x * cos) - (y * sin) + offsetX;
                int newY = (x * sin) + (y * cos) + offsetY;
                newTiles[(newX) + (newY) * 4] = tile[x + y * 4];
            }
        }
        return newTiles;
    }
    
    @Override
    public boolean equals(Object o1) {
        State2048 state = (State2048) o1;
        for (int i = 0; i < board.length; i++) {
            if (board[i].value != state.board[i].value) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < board.length; i++) {
            s += board[i].value;
            if ((i+1) %4 == 0) {
                s += "\n";
            }
        }
        return s;
    }
    
    public Tile[] getBoard() {
        return board;
    }

    public void setBoard(Tile[] board) {
        this.board = board;
    }
}
