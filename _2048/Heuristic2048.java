package _2048;

import AI.Heuristic;
import AI.State;
import java.util.ArrayList;
import java.util.List;

public class Heuristic2048 extends Heuristic {
    
    public double maxValue(State boardState) {
        State2048 state = (State2048)boardState;
        int bestValue = 2;
        for (Tile tile : state.getBoard()) {
            bestValue = tile.value > bestValue ? tile.value : bestValue;
        }
        return bestValue * 100;
    }
    
    public double totalValue(State boardState) {
        State2048 state = (State2048)boardState;
        int[] possibleNumber = {2,4,8,16,32,64,128,256,512,1024,2048,4096};
        int[] factor = {1,3,7,20,40,100,250,700,2000,5000,25000,100000};
        int totalValue = 0;
        for (Tile tile : state.getBoard()) {
            for (int i = 0; i < possibleNumber.length; i++) {
                if (possibleNumber[i] == tile.value) {
                    totalValue += factor[i];
                }
            }
        }
        return totalValue;
    }
    
    public double canMove(State boardState) {
        State2048 state = (State2048)boardState;
        if (!isFull(state)) {
            return 5000;
        }
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                Tile t = tileAt(x, y, state);
                if ((x < 3 && t.value == tileAt(x + 1, y,state).value)
                  || ((y < 3) && t.value == tileAt(x, y + 1,state).value)) {
                    return 5000;
                }
            }
        }
        return -100000;
    }
    
    private Tile tileAt(int x, int y, State2048 state) {
        return state.getBoard()[x + y * 4];
    }
    
    private List<Tile> availableSpace(State2048 state) {
        final List<Tile> list = new ArrayList<Tile>(16);
        for (Tile t : state.getBoard()) {
            if (t.isEmpty()) {
                list.add(t);
            }
        }
        return list;
    }

    private boolean isFull(State2048 state) {
        return availableSpace(state).size() == 0;
    }
    
    public int possibleMerge(State boardState) {
        State2048 state = (State2048)boardState;
        int possibleMerge = 0;
        for (int rows = 0; rows < 4; rows++) {
            int j = 0;
            int[]test = new int[4];
            for (int i = (4 * rows) + 0; i < (4 * rows) + 4; i++) {
                if (state.getBoard()[i].value != 0) {
                    test[j] = state.getBoard()[i].value;
                    j++;
                }
            }
            for (int i = 0; i < test.length-1; i++) {
                if (test[i] == test[i+1] && test[i] != 0) {
                    possibleMerge++;
                }
            }
        }
        for (int cols = 0; cols < 4; cols++) {
            int j =0;
            int[]test = new int[4];
            for (int i = 0 + cols; i < 13 + cols; i += 4) {
                if (state.getBoard()[i].value != 0) {
                    test[j] = state.getBoard()[i].value;
                    j++;
                }
            }
            for (int i = 0; i < test.length-1; i++) {
                if (test[i] == test[i+1] && test[i] != 0) {
                    possibleMerge++;
                }
            }
        }
        return possibleMerge * 5000;
    }
    
    public int distanceFromCornor(State boardState) {
        State2048 state = (State2048)boardState;
        int[] bestValueList = new int[4];
        int distance = 0;
        for (int j = 0; j < bestValueList.length; j++) {
            int bestValue = 0;
            for (int i = 0; i < state.getBoard().length; i++) {
                if (state.getBoard()[i].value > bestValue) {
                    boolean contains = false;
                    for (int value : bestValueList) {
                        if (value == state.getBoard()[i].value) {
                            contains = true;
                        }
                    }
                    if (!contains) {
                        bestValue = state.getBoard()[i].value; 
                    }
                }
            }
            bestValueList[j] = bestValue;
        }
        int i = 0;
        int value = 0;
        int j = 0;
        for (int bestValue : bestValueList) {
            if (bestValue != 0) {
                if (i == 0) {
                    int dist = closestFromN(bestValue,0,state);
                    if (closestFromN(bestValue,3,state) < dist) {
                        dist = closestFromN(bestValue,3,state);
                        value = 3;
                    }
                    if (closestFromN(bestValue,12,state) < dist) {
                        dist = closestFromN(bestValue,12,state);
                        value = 12;
                    }
                    if (closestFromN(bestValue,15,state) < dist) {
                        dist = closestFromN(bestValue,15,state);
                        value = 15;
                    }
                    distance += dist;
                } else {
                    int dist = 0;
                    switch (value) {
                        case 0:
                            dist = closestFromN(bestValue,value + i,state);
                            if ((closestFromN(bestValue,value + (i*4),state) < dist && i == 1) || j == 1) {
                                dist = closestFromN(bestValue,value + (i*4),state);
                                if (i == 1) {
                                    j = 1;
                                }
                            } 
                        case 3:
                            dist = closestFromN(bestValue,value - i,state);
                            if ((closestFromN(bestValue,value + (i*4),state) < dist && i == 1) || j == 1) {
                                dist = closestFromN(bestValue,value + (i*4),state);
                                if (i == 1) {
                                    j = 1;
                                }
                            } 
                        case 12:
                            dist = closestFromN(bestValue,value + i,state);
                            if ((closestFromN(bestValue,value - (i*4),state)  < dist && i == 1) || j == 1) {
                                dist = closestFromN(bestValue,value - (i*4),state);
                                if (i == 1) {
                                    j = 1;
                                }
                            }     
                        case 15:
                            dist = closestFromN(bestValue,value - i,state);
                            if ((closestFromN(bestValue,value + (i*4),state) < dist && i == 1) || j == 1) {
                                dist = closestFromN(bestValue,value + (i*4),state);
                                if (i == 1) {
                                    j = 1;
                                }
                            }     
                    }
                    distance += dist;
                }
            }
            i++;
        }
        return distance * 2000;
    }
    
    private int closestFromN (int n, int pos, State2048 state) {
        int[] allNPos = new int[16];
        for (int i = 0; i < state.getBoard().length; i++) {
            if (state.getBoard()[i].value == n) {
                allNPos[i] = n;
            }
        }
        int shortestDistance = 16;
        for (int i = 0; i < allNPos.length; i++) {
            if (allNPos[i] != 0) {
                if (Math.abs((i % 4)- pos)  + (i / 4) < shortestDistance) {
                    shortestDistance = Math.abs((i % 4)- pos)  + (i / 4);
                }
            }
        }
        return shortestDistance;
    }
    
    public int monotonic(State boardState) {
        State2048 state = (State2048)boardState;
        int value = 0;
        for (int rows = 0; rows < 4; rows++) {
            int j = 0;
            int[]test = new int[4];
            for (int i = (4 * rows) + 0; i < (4 * rows) + 4; i++) {
                if (state.getBoard()[i].value != 0) {
                    test[j] = state.getBoard()[i].value;
                    j++;
                }
            }
            if (!isAscending(test) && !isDescending(test)) {
                value++;
            }
        }
        for (int cols = 0; cols < 4; cols++) {
            int j =0;
            int[]test = new int[4];
            for (int i = 0 + cols; i < 13 + cols; i += 4) {
                if (state.getBoard()[i].value != 0) {
                    test[j] = state.getBoard()[i].value;
                    j++;
                }
            }
            if (!isAscending(test) && !isDescending(test)) {
                value++;
            }
        }
        return value * 5000;
    }
    
    public boolean isAscending(int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            if (tab[i] > tab[i+1] && tab[i] != 0 && tab[i+1] != 0)
                return false;
        }
        return true;
    }
    
    public boolean isDescending(int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            if (tab[i] < tab[i+1] && tab[i] != 0 && tab[i+1] != 0)
                return false;
        }
        return true;
    }
}
