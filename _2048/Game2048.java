package _2048;


/*
 * Copyright 1998-2014 Konstantin Bulenkov http://bulenkov.com/about
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import AI.Decision;
import AI.GeneticAI;
import AI.Population;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Konstantin Bulenkov
 */
public class Game2048 extends JPanel {
  private static final Color BG_COLOR = new Color(0xbbada0);
  private static final String FONT_NAME = "Arial";
  private static final int TILE_SIZE = 64;
  private static final int TILES_MARGIN = 16;

  private State2048 state;
  private GeneticAI AI;
  boolean myWin = false;
  boolean myLose = false;
  int myScore = 0;
  int genomeScore = 0;
  int bestScore = 0;
  int counter = 0;

  public Game2048(GeneticAI AI) {
    this.AI = AI;
    setPreferredSize(new Dimension(340, 400));
    setFocusable(true);
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        
        if (!canMove()) {
          myLose = true;
          System.out.println("Generation : " + AI.getGenerationNumber() + "     Child : "
                  + (int)Math.ceil((double)AI.getChildNumber() / AI.getGamePerGenome()));
          System.out.println("Score : " + myScore);
          System.out.println(printHighestValue());
          genomeScore += myScore;
          counter++;
          if (counter > 5) {
              counter = 0;
              genomeScore = 0;
              if (genomeScore >  bestScore) {
                  bestScore = genomeScore;
                  saveBestValue();
              }
          }
          AI.nextGame(myScore);
          resetGame();
        }
        
        
        
        if (!myWin && !myLose) {
            switch (e.getKeyCode()) {
              case KeyEvent.VK_A:
                left();
                break;
              case KeyEvent.VK_D:
                right();
                break;
              case KeyEvent.VK_S:
                down();
                break;
              case KeyEvent.VK_W:
                up();
                break;
            }
            
        }
        if (!myWin && !canMove()) {
            myLose = true;
            System.out.println("Generation : " + AI.getGenerationNumber() + "     Child : " 
                    + (int)Math.ceil((double)AI.getChildNumber() / AI.getGamePerGenome()));
            System.out.println("Score : " + myScore);
            System.out.println(printHighestValue());
            genomeScore += myScore;
            counter++;
            if (counter > 5) {
              if (genomeScore >  bestScore) {
                  bestScore = genomeScore;
                  saveBestValue();
              }
              counter = 0;
              genomeScore = 0;
            }
            AI.nextGame(myScore);
            resetGame();
            
        }
        
            state = new State2048(state);
            State2048 testState = state.getRight();
            Decision decision = AI.takeDecision(state);
            decision.executeKeyPress();
        
        repaint();
      }
    });
    resetGame();
  }

  public void resetGame() {
    
    myScore = 0;
    myWin = false;
    myLose = false;
    Tile[] tiles = new Tile[4 * 4];
    for (int i = 0; i < tiles.length; i++) {
      tiles[i] = new Tile();
    }
    state = new State2048(tiles);
    addTile();
    addTile();
    
  }

  public void left() {
    boolean needAddTile = false;
    for (int i = 0; i < 4; i++) {
      Tile[] line = getLine(i);
      Tile[] merged = mergeLine(moveLine(line));
      setLine(i, merged);
      if (!needAddTile && !compare(line, merged)) {
        needAddTile = true;
      }
    }

    if (needAddTile) {
      addTile();
    }
  }

  public void right() {
    state.setBoard(state.rotate(180,state.getBoard()));
    left();
    state.setBoard(state.rotate(180,state.getBoard()));
  }

  public void up() {
    state.setBoard(state.rotate(270,state.getBoard()));
    left();
    state.setBoard(state.rotate(90,state.getBoard()));
  }

  public void down() {
    state.setBoard(state.rotate(90,state.getBoard()));
    left();
    state.setBoard(state.rotate(270,state.getBoard()));
  }

  private Tile tileAt(int x, int y) {
    return state.getBoard()[x + y * 4];
  }

  private void addTile() {
    List<Tile> list = availableSpace();
    if (!availableSpace().isEmpty()) {
      int index = (int) (Math.random() * list.size()) % list.size();
      Tile emptyTime = list.get(index);
      emptyTime.value = Math.random() < 0.9 ? 2 : 4;
    }
  }

  private List<Tile> availableSpace() {
    final List<Tile> list = new ArrayList<Tile>(16);
    for (Tile t : state.getBoard()) {
      if (t.isEmpty()) {
        list.add(t);
      }
    }
    return list;
  }

  private boolean isFull() {
    return availableSpace().size() == 0;
  }

  boolean canMove() {
    if (!isFull()) {
      return true;
    }
    for (int x = 0; x < 4; x++) {
      for (int y = 0; y < 4; y++) {
        Tile t = tileAt(x, y);
        if ((x < 3 && t.value == tileAt(x + 1, y).value)
          || ((y < 3) && t.value == tileAt(x, y + 1).value)) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean compare(Tile[] line1, Tile[] line2) {
    if (line1 == line2) {
      return true;
    } else if (line1.length != line2.length) {
      return false;
    }

    for (int i = 0; i < line1.length; i++) {
      if (line1[i].value != line2[i].value) {
        return false;
      }
    }
    return true;
  }

  

  private Tile[] moveLine(Tile[] oldLine) {
    LinkedList<Tile> l = new LinkedList<Tile>();
    for (int i = 0; i < 4; i++) {
      if (!oldLine[i].isEmpty())
        l.addLast(oldLine[i]);
    }
    if (l.size() == 0) {
      return oldLine;
    } else {
      Tile[] newLine = new Tile[4];
      ensureSize(l, 4);
      for (int i = 0; i < 4; i++) {
        newLine[i] = l.removeFirst();
      }
      return newLine;
    }
  }

  private Tile[] mergeLine(Tile[] oldLine) {
    LinkedList<Tile> list = new LinkedList<Tile>();
    for (int i = 0; i < 4 && !oldLine[i].isEmpty(); i++) {
      int num = oldLine[i].value;
      if (i < 3 && oldLine[i].value == oldLine[i + 1].value) {
        num *= 2;
        myScore += num;
        int ourTarget = 8192;
        if (num == ourTarget) {
          myWin = true;
        }
        i++;
      }
      list.add(new Tile(num));
    }
    if (list.size() == 0) {
      return oldLine;
    } else {
      ensureSize(list, 4);
      return list.toArray(new Tile[4]);
    }
  }

  private static void ensureSize(java.util.List<Tile> l, int s) {
    while (l.size() != s) {
      l.add(new Tile());
    }
  }

  private Tile[] getLine(int index) {
    Tile[] result = new Tile[4];
    for (int i = 0; i < 4; i++) {
      result[i] = tileAt(i, index);
    }
    return result;
  }

  private void setLine(int index, Tile[] re) {
    System.arraycopy(re, 0, state.getBoard(), index * 4, 4);
  }

  @Override
  public void paint(Graphics g) {
    super.paint(g);
    g.setColor(BG_COLOR);
    g.fillRect(0, 0, this.getSize().width, this.getSize().height);
    for (int y = 0; y < 4; y++) {
      for (int x = 0; x < 4; x++) {
        drawTile(g, state.getBoard()[x + y * 4], x, y);
      }
    }
  }
  
  public int printHighestValue() {
      int value = 2;
      for (Tile tile : state.getBoard()) {
          if (tile.value > value) {
              value = tile.value;
          }
      }
      if (value >= 2048) {
          saveHightestValue(value);
      }
      return value;
  }
  
  public void saveHightestValue(int maxValue) {
        PrintWriter writer = null;
      try {
          writer = new PrintWriter(new BufferedWriter(new FileWriter("bestScore.txt", true)));
          writer.println("Generation : " + AI.getGenerationNumber() + "     Child : "
                  + (int)Math.ceil((double)AI.getChildNumber() / AI.getGamePerGenome()));
          writer.println("Total value weight : " + ((Genome2048)AI.getCurrentGenome()).getTotalValueWeight());
          writer.println("Max value weight : " + ((Genome2048)AI.getCurrentGenome()).getMaxValueWeight());
          writer.println("Possible merge weight : " + ((Genome2048)AI.getCurrentGenome()).getPossibleMerge());
          writer.println("Distance from cornor weight : " + ((Genome2048)AI.getCurrentGenome()).getDistanceFromCornor());
          writer.println("Can move weight : " + ((Genome2048)AI.getCurrentGenome()).getCanMoveWeight());
          writer.println("Monotonic weight : " + ((Genome2048)AI.getCurrentGenome()).getMonotonicWeight());
          writer.println("Score : " + myScore);
          writer.println("Max value : " + maxValue);
          writer.close();
      } catch (IOException ex) {
          
      } finally {
          writer.close();
      }
  }
  
  public void saveBestValue() {
        PrintWriter writer = null;
      try {
          writer = new PrintWriter("highestScore.txt", "UTF-8");
          writer.println("Generation : " + AI.getGenerationNumber() + "     Child : "
                  + (int)Math.ceil((double)AI.getChildNumber() / AI.getGamePerGenome()));
          writer.println("Total value weight : " + ((Genome2048)AI.getCurrentGenome()).getTotalValueWeight());
          writer.println("Max value weight : " + ((Genome2048)AI.getCurrentGenome()).getMaxValueWeight());
          writer.println("Possible merge weight : " + ((Genome2048)AI.getCurrentGenome()).getPossibleMerge());
          writer.println("Distance from cornor weight : " + ((Genome2048)AI.getCurrentGenome()).getDistanceFromCornor());
          writer.println("Can move weight : " + ((Genome2048)AI.getCurrentGenome()).getCanMoveWeight());
          writer.println("Monotonic weight : " + ((Genome2048)AI.getCurrentGenome()).getMonotonicWeight());
          writer.println("Score : " + bestScore);
          writer.close();
      } catch (IOException ex) {
          
      } finally {
          writer.close();
      }
  }

  private void drawTile(Graphics g2, Tile tile, int x, int y) {
    Graphics2D g = ((Graphics2D) g2);
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
    int value = tile.value;
    int xOffset = offsetCoors(x);
    int yOffset = offsetCoors(y);
    g.setColor(tile.getBackground());
    g.fillRoundRect(xOffset, yOffset, TILE_SIZE, TILE_SIZE, 14, 14);
    g.setColor(tile.getForeground());
    final int size = value < 100 ? 36 : value < 1000 ? 32 : 24;
    final Font font = new Font(FONT_NAME, Font.BOLD, size);
    g.setFont(font);

    String s = String.valueOf(value);
    final FontMetrics fm = getFontMetrics(font);

    final int w = fm.stringWidth(s);
    final int h = -(int) fm.getLineMetrics(s, g).getBaselineOffsets()[2];

    if (value != 0)
      g.drawString(s, xOffset + (TILE_SIZE - w) / 2, yOffset + TILE_SIZE - (TILE_SIZE - h) / 2 - 2);

    if (myWin || myLose) {
      g.setColor(new Color(255, 255, 255, 30));
      g.fillRect(0, 0, getWidth(), getHeight());
      g.setColor(new Color(78, 139, 202));
      g.setFont(new Font(FONT_NAME, Font.BOLD, 48));
      if (myWin) {
        g.drawString("You won!", 68, 150);
      }
      if (myLose) {
        g.drawString("Game over!", 50, 130);
        g.drawString("You lose!", 64, 200);
      }
      if (myWin || myLose) {
        g.setFont(new Font(FONT_NAME, Font.PLAIN, 16));
        g.setColor(new Color(128, 128, 128, 128));
        g.drawString("Press ESC to play again", 80, getHeight() - 40);
      }
    }
    g.setFont(new Font(FONT_NAME, Font.PLAIN, 18));
    g.drawString("Score: " + myScore, 200, 365);

  }

  private static int offsetCoors(int arg) {
    return arg * (TILES_MARGIN + TILE_SIZE) + TILES_MARGIN;
  }

  

  public static void main(String[] args) {
    JFrame game = new JFrame();
    game.setTitle("2048 Game");
    game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    game.setSize(340, 400);
    game.setResizable(false);
    
    //Genome2048 bestGenome = new Genome2048(360.767,230.311,0.713,-1.104,-1.013);
    
    
    Population population = new Population2048(100,5);
    
    GeneticAI AI = new GeneticAI(population);
    game.add(new Game2048(AI));
    
    game.setLocationRelativeTo(null);
    game.setVisible(true);
  }
}