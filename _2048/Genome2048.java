package _2048;

import AI.Genome;
import AI.State;
import java.util.HashSet;
import java.util.Random;

public class Genome2048 extends Genome {

    private double maxValueWeight;
    private double totalValueWeight;
    private double possibleMergeWeight;
    private double distanceFromCornor;
    private double canMoveWeight;
    private double monotonicWeight;
    
    public Genome2048() {
        Random rand = new Random();
        double weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        maxValueWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        totalValueWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        possibleMergeWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        canMoveWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        distanceFromCornor = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        monotonicWeight = weight;
    }
    
    public Genome2048(double totalValueWeight, double maxValueWeight,
            double possibleMergeWeight,  double distanceFromCornor,double canMoveWeight, double monotonicWeight) {
        this.maxValueWeight = maxValueWeight;
        this.totalValueWeight = totalValueWeight;
        this.possibleMergeWeight = possibleMergeWeight;
        this.canMoveWeight = canMoveWeight;
        this.distanceFromCornor = distanceFromCornor;
        this.monotonicWeight = monotonicWeight;
    }
    
    @Override
    public double calculateGenomeScore(State boardState) {
        double score = 0;
        Heuristic2048 heuristic = new Heuristic2048();
        HashSet<State2048> allNextState = getAllNextMoveNextMove((State2048)boardState);
        for (State2048 state : allNextState) {
            score += maxValueWeight * heuristic.maxValue(state);
            score += totalValueWeight * heuristic.totalValue(state);
            score += possibleMergeWeight * heuristic.possibleMerge(state);
            score += canMoveWeight * heuristic.canMove(state);
            score += monotonicWeight * heuristic.monotonic(state);
            score += distanceFromCornor * heuristic.distanceFromCornor(state);
        }
        score = score / allNextState.size();
        return score;
    }
    
    public HashSet<State2048> getAllNextMoveNextMove(State2048 state) {
        HashSet<State2048> allNextState = new HashSet<>();
        for (int i = 0; i < state.getBoard().length; i++) {
            if (state.getBoard()[i].value == 0) {
                State2048 newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 2;
                allNextState.addAll(getAllNextMove(newState.getLeft()));
                newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 4;
                allNextState.addAll(getAllNextMove(newState.getLeft()));
                newState.getBoard()[i].value = 2;
                allNextState.addAll(getAllNextMove(newState.getRight()));
                newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 4;
                allNextState.addAll(getAllNextMove(newState.getRight()));
                newState.getBoard()[i].value = 2;
                allNextState.addAll(getAllNextMove(newState.getTop()));
                newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 4;
                allNextState.addAll(getAllNextMove(newState.getTop()));
                newState.getBoard()[i].value = 2;
                allNextState.addAll(getAllNextMove(newState.getBottom()));
                newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 4;
                allNextState.addAll(getAllNextMove(newState.getBottom()));
                newState.getBoard()[i].value = 0;
            }
        }
        return allNextState;
    }
    
    public HashSet<State2048> getAllNextMove(State2048 state) {
        HashSet<State2048> allNextState = new HashSet<>();
        for (int i = 0; i < state.getBoard().length; i++) {
            if (state.getBoard()[i].value == 0) {
                State2048 newState = new State2048(state.getBoard());
                newState.getBoard()[i].value = 2;
                allNextState.add(newState);
                newState.getBoard()[i].value = 4;
                allNextState.add(newState);
                newState.getBoard()[i].value = 0;
            }
        }
        return allNextState;
    }

    @Override
    public void mutation() {
        Random rand = new Random();
        if (rand.nextInt(100) < 5) 
            maxValueWeight += rand.nextInt(400) -200 / 1000;
        if (rand.nextInt(100) < 5) 
            totalValueWeight += rand.nextInt(400) -200 / 1000;
        if (rand.nextInt(100) < 5) 
            possibleMergeWeight += rand.nextInt(400) -200 / 1000;
        if (rand.nextInt(100) < 5) 
            canMoveWeight += rand.nextInt(400) -200 / 1000;
        if (rand.nextInt(100) < 5) 
            distanceFromCornor += rand.nextInt(400) -200 / 1000;
        if (rand.nextInt(100) < 5) 
            monotonicWeight += rand.nextInt(400) -200 / 1000;
    }

    @Override
    public Genome crossbreeding(Genome genome) {
        int[] weightChoose = new int[6];
        Random rand = new Random();
        Genome2048 mother = (Genome2048)genome;
        for (int i = 0; i < weightChoose.length; i++) {
            weightChoose[i] = rand.nextInt(2);
        } 
        Genome2048 newGenome = new Genome2048();
        newGenome.maxValueWeight = weightChoose[0] == 0 ? maxValueWeight : mother.maxValueWeight;
        newGenome.totalValueWeight = weightChoose[1] == 0 ? totalValueWeight : mother.totalValueWeight;
        newGenome.possibleMergeWeight = weightChoose[2] == 0 ? possibleMergeWeight : mother.possibleMergeWeight;
        newGenome.canMoveWeight = weightChoose[3] == 0 ? canMoveWeight : mother.canMoveWeight;
        newGenome.distanceFromCornor = weightChoose[4] == 0 ? distanceFromCornor : mother.distanceFromCornor;
        newGenome.monotonicWeight = weightChoose[5] == 0 ? monotonicWeight : mother.monotonicWeight;
        return newGenome;
    }

    public double getMaxValueWeight() {
        return maxValueWeight;
    }

    public double getTotalValueWeight() {
        return totalValueWeight;
    }

    public double getCanMoveWeight() {
        return canMoveWeight;
    }
    
    public double getPossibleMerge() {
        return possibleMergeWeight;
    }

    public double getDistanceFromCornor() {
        return distanceFromCornor;
    }

    public double getMonotonicWeight() {
        return monotonicWeight;
    }
}
