package _2048;

import AI.Population;

public class Population2048 extends Population {
    
    public Population2048(int populationSize, int gamePerGenome) {
        super(populationSize, gamePerGenome);
        for (int i = 0; i < populationSize; i++) {
            genomes[i]  = new Genome2048();
        }
    }
    
}
