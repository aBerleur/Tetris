package _2048;

import AI.Decision;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class Decision2048 implements Decision {
    private char move;
    
    public Decision2048(char move) {
        this.move = move;
    }
    
    public char[] translateToKey() {
        char[] key = new char[1];
        key[0] = move;
        return key; 
    }

    @Override
    public void executeKeyPress() {
        char[] keys = translateToKey();
        try{
            try {
                TimeUnit.MILLISECONDS.sleep(0);
            } catch (InterruptedException ex) {
                
            }
            Robot robot = new Robot();
            for(char c : keys){
                if(c == 'D'){
                    robot.keyPress(KeyEvent.VK_D);
                    robot.keyRelease(KeyEvent.VK_D);
                }else if(c == 'A'){
                    robot.keyPress(KeyEvent.VK_A);
                    robot.keyRelease(KeyEvent.VK_A);
                }else if(c == 'W'){
                    robot.keyPress(KeyEvent.VK_W);
                    robot.keyRelease(KeyEvent.VK_W);
                }else if(c == 'S'){
                    robot.keyPress(KeyEvent.VK_S);
                    robot.keyRelease(KeyEvent.VK_S);
                }
            }
        }catch(AWTException e){
            e.printStackTrace();
        }
    }
}
