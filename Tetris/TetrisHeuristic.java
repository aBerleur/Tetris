package Tetris;

import AI.Heuristic;
import AI.State;

public class TetrisHeuristic extends Heuristic {
    
    // Testé
    public double aggregateHeight(State boardState) {
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double height = 0;
        
        for(int x = 0; x < board[0].length; x++){
            for(int y = 0; y < board.length; y++){
                if(board[y][x] != null){
                    height += board.length - y;
                    break;
                }
            }
        }

        return height;
    }
    
    // Testé
    public double completeLines(State boardState) {
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double lines = 0;
        
        for(int y = 0; y < board.length; y++){
            for(int x = 0; x < board[0].length; x++){
                if(board[y][x] == null)
                    break;
                if(x == board[0].length-1)
                    lines++;
            }
        }

        return lines;
    }
    
    // Testé
    public double holes(State boardState) {
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double holes = 0;
        
        for(int y = 1; y < board.length; y++){
            for(int x = 0; x < board[0].length; x++){
                if(board[y][x] == null && board[y-1][x] != null)
                    holes++;
            }
        }

        return holes;
    }
    
    // Testé
    public double bumpiness(State boardState) {
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double bumpiness = 0;
        double[] height = new double[board[0].length];

        for(int x = 0; x < board[0].length; x++){
            for(int y = 0; y < board.length; y++){
                if(board[y][x] != null){
                    height[x] = board.length - y;
                    break;
                }
            }
        }

        for(int i = 0; i < height.length-1; i++)
            bumpiness += Math.abs(height[i] - height[i+1]);

        return bumpiness;
    }

    public double maxHeight(State boardState){
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double maxHeight = 0;
        
        for(int x = 0; x < board[0].length; x++){
            for(int y = 0; y < board.length; y++){
                if(board[y][x] != null){
                    maxHeight = (board.length - y) > maxHeight ? (board.length - y) : maxHeight;
                    break;
                }
            }
        }

        return maxHeight;
    }

    public double relativeHeight(State boardState){
        TetrisState state = (TetrisState)boardState;
        TileType[][] board = state.getBoard();
        double maxHeight = 0;
        double minHeight = 99;
        
        for(int x = 0; x < board[0].length; x++){
            for(int y = 0; y < board.length; y++){
                if(board[y][x] != null){
                    maxHeight = (board.length - y) > maxHeight ? (board.length - y) : maxHeight;
                    minHeight = (board.length - y) < minHeight ? (board.length - y) : minHeight;
                    break;
                }
            }
        }

        return maxHeight - minHeight;
    }
}
