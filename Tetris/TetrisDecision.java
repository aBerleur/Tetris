package Tetris;

import AI.Decision;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class TetrisDecision implements Decision{
    //positif - moveG ; negatif = moveG
    private int move;
    //positif = rotationD ; negatif = rotationG
    private int rotation;

    public TetrisDecision(int move, int rotation) {
        this.move = move;
        this.rotation = rotation;
    }
    
    @Override
    public void executeKeyPress(){
        char[] keys = translateToKey();
        
        try{
            Robot robot = new Robot();
            for(char c : keys){
                if(c == 'D'){
                    robot.keyPress(KeyEvent.VK_D);
                    robot.keyRelease(KeyEvent.VK_D);
                }else if(c == 'A'){
                    robot.keyPress(KeyEvent.VK_A);
                    robot.keyRelease(KeyEvent.VK_A);
                }else if(c == 'E'){
                    robot.keyPress(KeyEvent.VK_E);
                    robot.keyRelease(KeyEvent.VK_E);
                }else if(c == 'Q'){
                    robot.keyPress(KeyEvent.VK_Q);
                    robot.keyRelease(KeyEvent.VK_Q);
                }
            }
        }catch(AWTException e){
            e.printStackTrace();
        }
    }

    @Override
    public char[] translateToKey() {
        char[] key = new char[Math.abs(move) + Math.abs(rotation)];
        int i = 0;
        int temp = rotation;
        if (temp > 0) {
            while (temp > 0) {
                temp--;
                key[i] = 'E';
                i++;
            }
        } else {
            while (temp < 0) {
                temp++;
                key[i] = 'Q';
                i++;
            }
        }
        temp = move;
        if (temp > 0) {
            while (temp > 0) {
                temp--;
                key[i] = 'D';
                i++;
            }
        } else {
            while (temp < 0) {
                temp++;
                key[i] = 'A';
                i++;
            }
        }
        return key;
    }

    
}
