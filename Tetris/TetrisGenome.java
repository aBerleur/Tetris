package Tetris;

import AI.Genome;
import AI.State;
import java.util.Random;

public class TetrisGenome extends Genome {
    private double aggregateHeightWeight;
    private double completeLinesWeight;
    private double holesWeight;
    private double bumpinessWeight;
    private double maxHeightWeight;
    private double relativeHeightWeight;
 
    public TetrisGenome() {
        Random rand = new Random();
        double weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        aggregateHeightWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        completeLinesWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        holesWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        bumpinessWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        maxHeightWeight = weight;
        weight = (double)(rand.nextInt(4000) - 2000)/1000.0;
        relativeHeightWeight = weight;
    }
    
    public TetrisGenome(double aggregateHeightWeight,double completeLinesWeight
            , double holesWeight, double bumpinessWeight,double maxHeightWeight, double relativeHeightWeight) {
        this.aggregateHeightWeight = aggregateHeightWeight;
        this.bumpinessWeight = bumpinessWeight;
        this.holesWeight = holesWeight;
        this.completeLinesWeight = completeLinesWeight;
        this.maxHeightWeight = maxHeightWeight;
        this.relativeHeightWeight = relativeHeightWeight;
    }
    
    public double calculateGenomeScore(State boardState) {
        int score = 0;
        TetrisHeuristic heuristic = new TetrisHeuristic();
        score +=  aggregateHeightWeight * heuristic.aggregateHeight(boardState);
        score +=  completeLinesWeight * heuristic.completeLines(boardState);
        score +=  holesWeight * heuristic.holes(boardState);
        score +=  bumpinessWeight * heuristic.bumpiness(boardState);
        score +=  maxHeightWeight * heuristic.maxHeight(boardState);
        score +=  relativeHeightWeight * heuristic.relativeHeight(boardState);
        
        return score;
    }
    
    @Override
    public void mutation() {
        Random rand = new Random();
        if(rand.nextInt(100) < 5)
            aggregateHeightWeight += rand.nextInt(400) - 200 / 1000;
        if(rand.nextInt(100) < 5)
            completeLinesWeight += rand.nextInt(400) - 200 / 1000;
        if(rand.nextInt(100) < 5)
            holesWeight += rand.nextInt(400) - 200 / 1000;
        if(rand.nextInt(100) < 5)
            bumpinessWeight += rand.nextInt(400) - 200 / 1000;
        if(rand.nextInt(100) < 5)
            maxHeightWeight += rand.nextInt(400) - 200 / 1000;
        if(rand.nextInt(100) < 5)
            relativeHeightWeight += rand.nextInt(400) - 200 / 1000;
    }

    @Override
    public Genome crossbreeding(Genome genome) {
        int[] weightChoose = new int[6];
        Random rand = new Random();
        TetrisGenome mother = (TetrisGenome)genome;
        for (int i = 0; i < weightChoose.length; i++) {
            weightChoose[i] = rand.nextInt(2);
        } 
        TetrisGenome newGenome = new TetrisGenome();
        newGenome.aggregateHeightWeight = weightChoose[0] == 0 ? aggregateHeightWeight : mother.aggregateHeightWeight;
        newGenome.completeLinesWeight = weightChoose[1] == 0 ? completeLinesWeight : mother.completeLinesWeight;
        newGenome.holesWeight = weightChoose[2] == 0 ? holesWeight : mother.holesWeight;
        newGenome.bumpinessWeight = weightChoose[3] == 0 ? bumpinessWeight : mother.bumpinessWeight;
        newGenome.maxHeightWeight = weightChoose[4] == 0 ? maxHeightWeight : mother.maxHeightWeight;
        newGenome.relativeHeightWeight = weightChoose[5] == 0 ? relativeHeightWeight : mother.relativeHeightWeight;
        return newGenome;
    }

    @Override
    public String toString(){
        String string = "AggregateHeight: " + aggregateHeightWeight + "\n" +
                        "CompleteLine: " + completeLinesWeight + "\n" +
                        "Holes: " + holesWeight + "\n" + 
                        "Bumpiness: " + bumpinessWeight + "\n" + 
                        "MaxHeight: " + maxHeightWeight + "\n" + 
                        "RelativeHeight: " + relativeHeightWeight;
        return string;
    }
}
