package Tetris;

import AI.Population;

public class TetrisPopulation extends Population {
    
    public TetrisPopulation(int populationSize, int gamePerGenome) {
        super(populationSize, gamePerGenome);
        for (int i = 0; i < populationSize; i++) {
            genomes[i]  = new TetrisGenome();
        }
    }
    
}
