package Tetris;

import AI.Decision;
import AI.State;
import java.util.ArrayList;
import java.util.List;

public class TetrisState implements State {

    private TileType[][] board;
    private TileType currentTile;
    private TileType nextTile;
    
    public TetrisState(TileType[][] board, TileType currentTile, TileType nextTile) {
        this.board = board;
        this.currentTile = currentTile;
        this.nextTile = nextTile;
    }
    
    @Override
    public List<State> getAllPossibleState() {
        //Retourne tout les etats possible sachant le board et les 2 pieces futurs
        List<State> allFuturState = new ArrayList<State>();
        List<TetrisState> currentTurn = getAllPossibleStateWithTile(this, currentTile);
        if(currentTile != nextTile){
            for(TetrisState s : currentTurn){
                List<TetrisState> nextTurn = getAllPossibleStateWithTile(s, nextTile);
                allFuturState.addAll(nextTurn);
            }
        }else{
            allFuturState.addAll(currentTurn);
        }
        return allFuturState;
    }

    private List<TetrisState> getAllPossibleStateWithTile(TetrisState state, TileType tile){
        List<TetrisState> allFuturState = new ArrayList<TetrisState>();
        
        int maxRot = 4;
        if(tile.getDimension() == 2)
            maxRot = 1;
        
        // Pour chaque rotation possible
        for(int rot = 0; rot < maxRot; rot++){
            TetrisState nextState = state, previousState = state;
            int leftInset = tile.getLeftInset(rot);
            int rightInset = tile.getRightInset(rot);
            int bottomInset = tile.getBottomInset(rot);
            int topInset = tile.getTopInset(rot);

            // Pour chaque colonne possible
            for(int col = -leftInset; col < 10-tile.getDimension()+rightInset; col++){
                boolean collision = false;
                int row = 0+tile.getRows();
                while(row < 23-tile.getDimension()+bottomInset){
                    nextState = state.clone();
                    
                    for(int y = 0; y < tile.getDimension(); y++){
                        for(int x = 0; x < tile.getDimension(); x++){
                            if(tile.isTile(x,y,rot)){
                                if(col+x < 0 || col+x > 9 || row+y < 0 || row+y > 21){
                                    collision = true;
                                    break;
                                }
                                if(nextState.board[row+y][col+x] == null){
                                    nextState.board[row+y][col+x] = tile;
                                }else{
                                    collision = true;
                                    break;
                                }
                            }
                        }
                    }

                    if(collision)
                        break;

                    previousState = nextState.clone();

                    row++;
                }
                if(collision)
                    allFuturState.add(previousState);
                //else
                //    allFuturState.add(nextState);
            }
        }
        //System.out.println("futurState: " + allFuturState.size());
        return allFuturState;       
    }
    
    @Override
    public Decision takeDecision(State finalState) {
        //return la actions necessaire pour obtenir l'etat final en partant de cette etat

        // Isolate piece
        TileType[][] isolateBoard = isolatePiece(this.board, ((TetrisState)finalState).board);
        boolean[][] piece = getPiece(isolateBoard);

        int rot = currentTile.getRot(piece);
        int mov = getMove(isolateBoard, rot);
        
        return new TetrisDecision(mov,rot);
    }

    private TileType[][] isolatePiece(TileType[][] a, TileType[][] b){
        TileType[][] result = new TileType[a.length][a[0].length];

        //System.out.println("");
        for(int y = 0; y < a.length; y++){
            for(int x = 0; x < a[0].length; x++){
                //System.out.print(b[y][x] + " ");
                if(a[y][x] != b[y][x] && b[y][x] == currentTile){
                    result[y][x] = b[y][x];
                }
            }
            //System.out.println("");
        }

        return result;
    }

    private int getMove(TileType[][] isolateBoard, int rot){
        int minX = 99;
        for(int y = 0; y < isolateBoard.length; y++){
            for(int x = 0; x < isolateBoard[0].length; x++){
                if(isolateBoard[y][x] != null){
                    minX = (x < minX) ? x : minX;
                    break;
                }
            }
        }
        int spawnCol = currentTile.getSpawnColumn();
        int leftInset = currentTile.getLeftInset(rot);

        minX -= leftInset;
        if(minX > 10)
            minX = 0;
        minX = spawnCol-minX;
        return 0 - minX;
    }

    private boolean[][] getPiece(TileType[][] board){
    
        boolean[][] piece = new boolean[currentTile.getDimension()][currentTile.getDimension()];
        int minX = 99;
        int minY = 99;

        for(int y = 0; y < board.length; y++){
            for(int x = 0; x < board[0].length; x++){
                if(board[y][x] != null){
                    minX = (x < minX) ? x : minX;
                    minY = (y < minY) ? y : minY;
                }
            }
        }

        for(int y = 0; y < currentTile.getDimension(); y++){
            for(int x = 0; x < currentTile.getDimension(); x++){
                if((y+minY) < 22 && x+minX < 10 && board[y+minY][x+minX] != null){
                    piece[y][x] = true;
                }
            }
        }

        return piece;
    }

    public TileType[][] getBoard(){
        return board;
    }

    public TetrisState clone(){
        TileType[][] boardClone = new TileType[board.length][board[0].length];
        
        for(int y = 1; y < board.length; y++){                                            
            for(int x = 0; x < board[0].length; x++){                                     
                boardClone[y][x] = board[y][x];
            }                                 
        }  

        return new TetrisState(boardClone, currentTile, nextTile);
    }
}
