# Intelligence Artificielle Tetris / 2048

## Description

L'application est une intelligence artificielle qui apprend à jouer au jeu de tetris et à celui de 2048.
Le but était d’avoir un algorithme génétique générique qui trouve un génome qui puisse jouer a tetris en perdant le moins possible et un qui permet d’obtenir un score de 2048 a chaque essaie au jeu 2048. Le focus du travail
était principalement sur le jeu tetris.

## Compilation

Pour lancer l'intelligence artificielle de Tetris, entrer la commande

``make tetris``

Pour lancer l'intelligence artificielle de Tetris, entrer la commande

``make 2048``

## Conclusion
Finalement, nous n’avons pas compl` etement atteint nos buts, dans le jeu de tetris, nous avons trouvé un
génome qui complète plus de 800 lignes avant de perdre. Dans le jeu 2048, nous avons trouvé plein de
génome qui peuvent atteindre 2048 mais comme le jeu est très aléatoire, il n’est pas capable de reproduire
ce score a chaque partie qu’il joue. Une technique probabiliste est probablement meilleur pour les jeux non
déterministe comme 2048.

## Auteurs
- Anthony Berleur 
- Étienne Paquette Perrier 

## Sources

- https://codemyroad.wordpress.com/2013/04/14/tetris-ai-the-near-perfect-player/
- https://github.com/PSNB92/Tetris
- https://github.com/bulenkov/2048
- https://fr.wikipedia.org/wiki/Tetris
- https://fr.wikipedia.org/wiki/2048_(jeu_vid%C3%A9o)
- http://jdlm.info/articles/2018/03/18/markov-decision-process-2048.html