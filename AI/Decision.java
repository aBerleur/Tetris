package AI;

public interface Decision {
   public char[] translateToKey();
   public void executeKeyPress();
}
