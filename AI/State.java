package AI;

import java.util.List;

public interface State {
    public List<State> getAllPossibleState();
    public Decision takeDecision(State finalState);
}
