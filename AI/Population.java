package AI;

import Tetris.TetrisGenome;
import java.util.Arrays;
import java.util.Random;

public class Population {
    
    protected Genome[] genomes;
    protected int gamePerGenome;
    protected int generationNumber;
    protected int childNumber;
    
    public Population(int populationSize,int gamePerGenome) {
        this.gamePerGenome = gamePerGenome;
        childNumber = 0;
        generationNumber = 1;
        genomes = new Genome[populationSize];
    }

    
    public Genome nextGenome() {
        childNumber++;
        int index = (int) Math.ceil((double)childNumber/(double)gamePerGenome);
        if (childNumber > genomes.length * gamePerGenome) {
            nextGeneration();
            return nextGenome();
        } 
        return genomes[index - 1];
    }
    
    public void nextGeneration() {
        childNumber = 0;
        generationNumber++;
        elimination();
        crossbreeding();
        mutation();
        for (Genome genome : genomes) {
            genome.fitness = 0;
        }
    }
    
    public void elimination() {
        Arrays.sort(genomes, new GenomeComparatorByFitness());
    }
    
    public void crossbreeding() {
        Random rand = new Random();
        int indexPercent = (int) (0.1 * genomes.length);
        for (int i = indexPercent; i < genomes.length - 1; i += 2) {
            Genome father = genomes[rand.nextInt(indexPercent)];
            Genome mother = genomes[rand.nextInt(indexPercent)];
            genomes[i] = father.crossbreeding(mother);
            genomes[i + 1] = mother.crossbreeding(father);
        }
    }
    
    public void mutation() {
        for (Genome genome : genomes) {
            genome.mutation();
        }
    }

    public int getGenerationNumber(){
        return generationNumber;
    }

    public int getChildNumber(){
        return childNumber;
    }

    public int getGamePerGenome() {
        return gamePerGenome;
    }
}
