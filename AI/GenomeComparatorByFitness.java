package AI;

import java.util.Comparator;

public class GenomeComparatorByFitness implements Comparator<Genome>{
    
    @Override
    public int compare(Genome o1, Genome o2) {
        return -1 * (o1.getFitness() - o2.getFitness());
    }
}
