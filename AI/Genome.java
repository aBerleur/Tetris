package AI;

public abstract class Genome {
     public int fitness = 0;
     public abstract double calculateGenomeScore(State etat);
     public abstract void mutation();
     public abstract Genome crossbreeding(Genome etat);
     
     public void addFitness(int fitness) {
        this.fitness += fitness; 
     }

    public int getFitness() {
        return fitness;
    }    
}
