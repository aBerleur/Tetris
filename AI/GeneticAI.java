package AI;

import java.util.List;

public class GeneticAI {
    
    private Genome currentGenome;
    private Population population;
    private boolean onlyOneGenome = false;
    
    public GeneticAI(Population population) {
        this.population = population;
        currentGenome = population.nextGenome();
    }
    
    public GeneticAI(Genome genome) {
        population = new Population(10,1);
        currentGenome = genome;
        onlyOneGenome = true;
    }
    
    public Decision takeDecision(State actualState) {
        List<State> futurState = actualState.getAllPossibleState();
        State bestState = futurState.get(0);
        double bestStateScore = currentGenome.calculateGenomeScore(bestState);
        for (State s : futurState) {
            double currentStateScore = currentGenome.calculateGenomeScore(s);
            if (currentStateScore > bestStateScore) {
                bestState = s;
                bestStateScore = currentStateScore;
            }
        }
        Decision decision = actualState.takeDecision(bestState);
        return decision;
    }
    
    public void nextGame(int currentGenomeFitness) {
        if (!onlyOneGenome) {
            currentGenome.addFitness(currentGenomeFitness);
            currentGenome = population.nextGenome();
        }
    }

    public int getGenerationNumber(){
        return population.getGenerationNumber();
    }

    public int getChildNumber(){
        return population.getChildNumber();
    }
    
    public int getGamePerGenome() {
        return population.getGamePerGenome();
    }

    public Genome getCurrentGenome() {
        return currentGenome;
    }
    
    
}
